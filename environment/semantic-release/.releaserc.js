module.exports = {
  tagFormat: "${version}",
  branches: [
    "master",
    "next",
    {
      name: "@(alpha|beta|rc)",
      prerelease: true,
    },
  ],
  preset: "angular",
  plugins: [
    [
      "@semantic-release/commit-analyzer",
      {
        parserOpts: {
          noteKeywords: ["BREAKING CHANGE", "BREAKING CHANGES"],
        },
        releaseRules: [
          { scope: "docs", release: false },
          { scope: "no-release", release: false },
        ],
      },
    ],
    [
      "@semantic-release/release-notes-generator",
      {
        parserOpts: {
          referenceActions: ["META"],
          noteKeywords: ["BREAKING CHANGE", "BREAKING CHANGES"],
        },
        writerOpts: {
          commitsSort: ["subject", "scope"],
        },
      },
    ],
    "@semantic-release/changelog",
    [
      "@semantic-release/npm",
      {
        npmPublish: !!process.env.NPM_TOKEN,
      },
    ],
    [
      "@semantic-release/git",
      {
        assets: [
          "CHANGELOG.md",
          "package.json",
          "package-lock.json",
          "npm-shrinkwrap.json",
          "yarn.lock",
          "pnpm-lock.yaml",
        ],
        message: "chore(release): ${nextRelease.version}\n${nextRelease.notes}",
      },
    ],
    "@semantic-release/gitlab",
  ],
};
