FROM debian:stable-20220822-slim

ARG USER=admin
ARG GROUP=admin
ARG UID=1000
ARG GID=1000
RUN groupadd -g ${GID} ${GROUP} \
    && useradd -u ${UID} -g ${GROUP} -s /bin/sh -m ${USER}
USER ${UID}:${GID}
WORKDIR /_templates
COPY . .