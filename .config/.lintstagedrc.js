module.exports = {
  "*.{md,yml,yaml,json}": ["npm run test:prettier -- --write"],

  "*.{ts,tsx,js,jsx}": ["npm run test:lint -- --fix"],
};
