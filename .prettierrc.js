module.exports = {
  overrides: [
    {
      files: "*.md",
      options: {
        printWidth: 120,
        proseWrap: "always",
      },
    },
  ],
};
